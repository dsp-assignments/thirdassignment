import ballerina/kafka;
import ballerina/log;

//producer

kafka:ProducerConfiguration producerConfiguration = {

    bootstrapServers: "localhost:2453",
    clientId: "NUSTPostgrad",
    acks: "all",
    retryCount: 3,

    valueSerializerType: kafka:SER_STRING,

    keySerializerType: kafka:SER_INT
};

kafka:Producer kafkaProducer = new (producerConfiguration);


public function main() returns error? {


string message = "Nust Postgrad Producer";
    var sendResult = kafkaProducer->send(message, Application", key = 1);
    if (sendResult is error) {
        log:printInfo("Kafka producer is experiencing some errors. " + sendResult.toString());
    } else {
        log:printInfo("Success");
    }
    var flushResult = kafkaProducer->flushRecords();
    if (flushResult is error) {
        log:printInfo("Kafka failed" + flushResult.toString());
    } else {
        log:printInfo("Successful.);
    }

} 
